package main

import (
	"fmt"

	"bitbucket.org/KimChiPls/bitcord/api"
)

func main() {
	api.Serve()
	fmt.Println("Server stopped")
}
