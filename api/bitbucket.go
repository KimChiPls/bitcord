package api

import (
	"encoding/json"
)

type BitBucketResponse struct {
	Push BitBucketPush `json:"push"`
	Repo BitBucketRepo `json:"repository"`
	Actor BitBucketActor `json:"actor"`
}

type BitBucketPush struct {
	Changes []BitBucketChange `json:"changes"`
}

type BitBucketChange struct {
	Commits []BitBucketCommit `json:"commits"`
	Links BitBucketLink `json:"links"`
}

type BitBucketCommit struct {
	Message string `json:"message"`
}

type BitBucketLink struct {
	Html BitBucketHtml `json:"html"`
}

type BitBucketHtml struct {
	Href string `json:"href"`
}

type BitBucketRepo struct {
	Name string `json:"name"`
}

type BitBucketActor struct {
	User string `json:"username"`
}

// NewDiscordResponse parses a valid BitBucket webhook push in to the BitBucketResponse struct.
func NewBitBucketResponse(msgBody []byte) BitBucketResponse {
	bbResp := BitBucketResponse{}
	json.Unmarshal(msgBody, &bbResp)
	return bbResp
}
