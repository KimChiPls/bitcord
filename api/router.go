package api

import (
	"github.com/gorilla/mux"
)

// initRoutes sets up handlers for MUX Router.
func initRoutes() {
	Router = mux.NewRouter()

	// Ping
	Router.HandleFunc("/ping", PingHandler).Name("ping")
	Router.HandleFunc("/ping/", PingHandler).Name("ping")

	// General Discord webhook
	Router.HandleFunc("/discord", DiscordHandler).Name("discord").Methods("POST")
	Router.HandleFunc("/discord/", DiscordHandler).Name("discord").Methods("POST")
}
