package api

import (
	"bytes"
	"encoding/json"
	"net/http"
)

type DiscordPush struct {
	Username string `json:"username"`
	Content string `json:"content"`
	Embeds []DiscordEmbed `json:"embeds"`
}

type DiscordEmbed struct {
	Title string `json:"title"`
	Description string `json:"description"`
	Url string `json:"url"`
}

// DiscordFromBitBucket demuxes a BitBucket structure in to a Discord structure.
func DiscordFromBitBucket(bbr BitBucketResponse) DiscordPush {
	embed := DiscordEmbed{
		Title: bbr.Repo.Name,
		Description: bbr.Push.Changes[0].Commits[0].Message,
		Url: bbr.Push.Changes[0].Links.Html.Href,
	}
	dp := DiscordPush{
		Username: "BitBucket",
		Content: "Push to " + bbr.Repo.Name + " by " + bbr.Actor.User,
		Embeds: []DiscordEmbed{embed},
	}
	return dp
}

// Send attempts to send a Discord message to a Discord webhook.
func (dp *DiscordPush) Send() error {
	// Decode struct in to JSON string
	jsonString, _ := json.Marshal(dp)
	// Setup request
	req, err := http.NewRequest("POST", *discordUrl, bytes.NewBuffer(jsonString))
	req.Header.Set("Content-Type", "application/json")
	// Start http client to send request
	c := &http.Client{}
	resp, err := c.Do(req)
	defer resp.Body.Close()
	if err != nil {
		return err
	}
	return nil
}
