package api

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"

	"github.com/gorilla/mux"
)

var (
	discordUrl = flag.String("discord", "", "Discord Webhook URL")
	port = flag.String("port", ":80", "Port to listen on for BitBucket webhook")

	Router *mux.Router
)

func init() {
	flag.Parse()
	initRoutes()
}

// Serve initiates a REST API listener to accept BitBucket webhooks.
func Serve() {
	// Subscribe to SIGINT
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)

	// Init server
	server := &http.Server {
		Addr:    *port,
		Handler: Router,
	}
	// Start server
	go server.ListenAndServe()
	fmt.Println("Listening and serving on " + server.Addr)

	<-stopChan // Wait for SIGINT
	fmt.Println("Stopping server...")
}
