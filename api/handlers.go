package api

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

// PingHandler responds with HTTP 200 on any request.
func PingHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("PING from " + r.RemoteAddr)
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(200)
	bytes := []byte("OK")
	w.Write(bytes)
}

// DiscordHandler forwards the BitBucket webhook to a valid Discord webhook URL.
func DiscordHandler(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	bbResp := NewBitBucketResponse(body)
	discordPush := DiscordFromBitBucket(bbResp)
	err := discordPush.Send()
	w.Header().Set("Content-Type", "text/plain")
	if err != nil {
		w.WriteHeader(500)
		bytes := []byte(err.Error())
		w.Write(bytes)
		return
	}
	w.WriteHeader(200)
	return
}
